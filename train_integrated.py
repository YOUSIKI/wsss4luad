import argparse
import os

import numpy as np
import torch
from PIL import Image
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from torchvision import transforms
from tqdm import tqdm

import dataset
import network
from utils.generate_CAM import generate_cam
from utils.metric import get_overall_valid_score

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--name", type=str, help="the experiment name", required=True)
    parser.add_argument("--batch_size", default=16, type=int, help="batch size")
    parser.add_argument("--epochs", default=15, type=int, help="max epochs")
    parser.add_argument("--lr", default=0.001, type=float, help="learning rate")
    parser.add_argument(
        "--resize", default=224, type=int, help="resize image to this size"
    )
    parser.add_argument(
        "--lr_step", default=5, type=int, help="the step of lr_scheduler"
    )
    parser.add_argument(
        "--lr_gamma", default=0.1, type=float, help="the gamma of lr_scheduler"
    )
    parser.add_argument(
        "--resume", default=None, type=str, help="resume from checkpoint"
    )
    args = parser.parse_args()

    writer = SummaryWriter(log_dir=f"runs/{args.name}")
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    net = network.scalenet101(
        structure_path="structures/scalenet101.json", ckpt="weights/scalenet101.pth"
    )

    if args.resume is not None:
        checkpoint = torch.load(args.resume, map_location="cpu")
        net.load_state_dict(checkpoint["model"])
        print("load checkpoint from", args.resume)

    net.to(device)

    train_transform = transforms.Compose(
        [
            transforms.Resize((args.resize, args.resize)),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ]
    )

    train_transform = transforms.Compose(
        [
            transforms.Resize((args.resize, args.resize)),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ]
    )
    train_dataset = dataset.DoubleLabelDataset(transform=train_transform)
    train_sampler = torch.utils.data.RandomSampler(train_dataset)
    train_loader = DataLoader(
        train_dataset,
        batch_size=args.batch_size,
        num_workers=4,
        sampler=train_sampler,
        drop_last=True,
        pin_memory=True,
    )
    print("training set size:", len(train_dataset))

    optimizer = torch.optim.SGD(
        net.parameters(), args.lr, momentum=0.9, weight_decay=1e-4
    )
    scheduler = torch.optim.lr_scheduler.StepLR(
        optimizer, step_size=args.lr_step, gamma=args.lr_gamma, verbose=True
    )
    criterion = torch.nn.BCEWithLogitsLoss(reduction="mean")
    criterion.to(device)

    valid_image_path = "data/valid_patches"
    valid_image_sample = os.path.join(valid_image_path, os.listdir(valid_image_path)[0])
    side_length = np.asarray(Image.open(valid_image_sample)).shape[0]
    print("side length: ", side_length)

    os.makedirs("modelstates", exist_ok=True)

    global_step = 0
    best_valid_score = 0

    for epoch in range(1, 1 + args.epochs):
        epoch_step = 0
        epoch_loss = 0.0
        epoch_correct = 0

        net.train()

        for images, labels in tqdm(train_loader):
            global_step += 1
            epoch_step += 1

            images = images.to(device)
            labels = labels.to(device)
            scores = net(images)
            loss = criterion(scores, labels.float())

            correct = 0
            scores = torch.sigmoid(scores)
            predict = torch.zeros_like(scores)
            predict[scores > 0.5] = 1
            predict[scores < 0.5] = 0
            current_correct = 0
            for k in range(len(predict)):
                if torch.equal(predict[k], labels[k]):
                    correct += 1

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            epoch_loss += loss.item()
            epoch_correct += correct

            writer.add_scalar("train/epoch", epoch, global_step)
            writer.add_scalar("train/loss_step", loss.item(), global_step)
            writer.add_scalar("train/acc_step", correct / args.batch_size, global_step)

        epoch_loss /= epoch_step
        epoch_correct /= epoch_step * args.batch_size

        print(
            f"Epoch [{epoch} / {args.epochs}], Train Loss: {epoch_loss:.4f}, Train Acc: {epoch_correct:.4f}"
        )

        scheduler.step()

        torch.save(
            {
                "model": net.state_dict(),
                "optimizer": optimizer.state_dict(),
                "scheduler": scheduler.state_dict(),
            },
            os.path.join("modelstates", f"{args.name}-epoch{epoch:04d}.pth"),
        )
        torch.save(
            {
                "model": net.state_dict(),
                "optimizer": optimizer.state_dict(),
                "scheduler": scheduler.state_dict(),
            },
            os.path.join("modelstates", f"{args.name}-latest.pth"),
        )

        print("validating...")
        net_cam = network.scalenet101_cam(structure_path="structures/scalenet101.json")
        pretrained = net.state_dict()
        pretrained = {k.replace("module.", ""): v for k, v in pretrained.items()}
        pretrained["fc1.weight"] = (
            pretrained["fc1.weight"].unsqueeze(-1).unsqueeze(-1).to(torch.float64)
        )
        pretrained["fc2.weight"] = (
            pretrained["fc2.weight"].unsqueeze(-1).unsqueeze(-1).to(torch.float64)
        )
        net_cam.load_state_dict(pretrained)
        generate_cam(
            net_cam,
            args.name,
            tuple((side_length, side_length // 3)),
            args.batch_size,
            "valid",
            args.resize,
        )
        valid_image_path = os.path.join("data", "valid_out_cam", args.name)
        valid_score = get_overall_valid_score(valid_image_path)
        print(f"valid score: {valid_score}")
        writer.add_scalar("valid/score", valid_score, global_step)
        if best_valid_score <= valid_score:
            torch.save(
                {
                    "model": net.state_dict(),
                    "optimizer": optimizer.state_dict(),
                    "scheduler": scheduler.state_dict(),
                },
                os.path.join("modelstates", f"{args.name}-best.pth"),
            )
